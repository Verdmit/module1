﻿using System;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            int a = 0, b = 0;
            int min;
            int[] Arr;
            var module = new Module1();
            
            Console.WriteLine("Введите а");
            a = Convert.ToInt32(Console.ReadLine());

            Arr = module.SwapItems(a, b);
            Console.WriteLine("Введите b");
            b = Convert.ToInt32(Console.ReadLine());

            Arr = module.SwapItems(a, b);
            min = module.GetMinimumValue(Arr);

            static void COUT(int[] ArrOut)
            {
                for (int i=0; i< ArrOut.Length; i++)
                {
                    Console.WriteLine(ArrOut[i]);
                }
            }

            Console.WriteLine("минимум = " + min);

            Console.WriteLine("Выводим массив");
            COUT(Arr);

            Console.ReadKey();
        }

        public int[] SwapItems(int a, int b)
        {
            return new int[] { b, a };
        }

        public int GetMinimumValue(int[] input)
        {
            int minimum = input[0];

            for (int i = 1; i < input.Length; i++)
            {
                if (input[i] < minimum)
                {
                    minimum = input[i];
                }
            }
            return minimum;
        }
    }
}
